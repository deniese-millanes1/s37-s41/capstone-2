const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true, "Product must have total amount."]
	},
	purchasedOn: [
		{
			user: {
				type: String,
				required: [true]
			},			
			product: {
				type: String,
				required: [true]
			}
			purchasedOn: {
				type: Date,
				default: new Date(),
			}
		}
	]
})


module.exports = mongoose.model("Order",orderSchema);