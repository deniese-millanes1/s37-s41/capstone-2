const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Product Name is required."]
	},
	description: {
		type: String,
	},
	price: {
		type: Number,
		required: [true, "Please enter the product's price."]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	isCreatedOn: {
		type: Date,
		default: new Date(),
	}
})


module.exports = mongoose.model("Product",productSchema);