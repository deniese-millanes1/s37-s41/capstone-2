const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth")


router.post("/register", (req,res) => {
	userController.userRegister(req.body).then(resultFromController => res.send(resultFromController));
	}
)


router.post("/login", (req,res) => {
	userController.userLogin(req.body).then(resultFromController => res.send(resultFromController));
	}
)


router.get("/details", auth.verify, (req,res) => {
	const userData = auth.decode(req.headers.authorization);	
	console.log(userData);

	userController.userDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController));
	}
)





module.exports = router;