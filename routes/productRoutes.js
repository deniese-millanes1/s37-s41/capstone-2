const express = require("express");
const router = express.Router();
const courseController = require("../controllers/productControllers");
const auth = require("../auth");


router.get("/all", auth.verify, (req,res) => {
 	courseController.activeProducts().then(resultFromController => res.send(resultFromController));
	}
)


router.get("/:productId", (req,res) => {
	courseController.specificProduct(req.params).then(resultFromController => res.send(resultFromController));
	}
)
