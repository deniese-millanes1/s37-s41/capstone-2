const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI1";


module.exports.createAccessToken = (user) => {
	const payload = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	return jwt.sign(payload, secret, {expiresIn: "7d"});
}


module.exports.verify = (req,res,next) => {
	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token);
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error,data) => {
				if (error) {
					return res.send("Authentication Failed");
				} else {
					next();
				}
			}
		)
	} else {
		return res.send("Authentication Failed")
	}
}


module.exports.decode = (token) => {
	if (typeof token !== "undefined") {
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data) => {
				if (error) {
					return null
				} else {
					return jwt.decode(token, {complete: true}).payload
				}
			}
		)
	} else {
		return null
	}
}