const bcrypt = require("bcrypt");
const User = require("../models/User");

module.exports.userRegister = (reqBody) => {
	let newUser = new User (
		{
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			password: bcrypt.hashSync(reqBody.password,7),
			mobileNo: reqBody.mobileNo,
		}
	)
	return User.find({email: reqBody.email}).then(result => {
		if(result.length>0) {
			return "The email address provided is already registered."
		} else {
			return newUser.save().then((user,error) => {
				if (error) {
					return "Error in registering a new user."
				} else {
					return `User ${reqBody.firstName} ${reqBody.lastName} created.`
				}
			})
		}
	})
}


module.exports.userLogin = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
			if (result == null) {
				return "Email address is not yet registered."
			} else {
				const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

				if (isPasswordCorrect == true) {
					return {"access token": auth.createAccessToken(result)}
				} else {
					return "Password is incorrect"
				} 
			}
		}
	)
}